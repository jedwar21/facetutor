/**
 * 
 */
package edu.westga.cs1302.facetutor.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.facetutor.controller.LoadStudentDataController;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;
import edu.westga.cs1302.facetutor.model.StudentDataMap;


/**
* WhenLoadStudents defines test cases for 
* the "load movie data from file" use case.
* 
* @author Jessica Edwards
* @version Spring, 2015
*/
public class WhenLoadStudents {
    
    private LoadStudentDataController testLoadController;
    private File testFile;
    private DataMap<String, StudentData> theMap;
    
    /**
     * Creates a new WhenAddAMovie object.
     */
    public WhenLoadStudents() {
        // no-op
    }
    
    /**
     * Set up method for the test cases.
     * @throws IOException if test file can't be read
     */
    @Before
    public void setUp() throws IOException {
        this.theMap = new StudentDataMap();
        this.testFile = new File(getClass().getResource("resources/movies.txt").getFile());
        this.testLoadController = new LoadStudentDataController(this.testFile, this.theMap);
        this.testLoadController.loadStudentDataToMap();
        
    }


    /**
     * Test case for loading student data.
     */
    @Test
    public void mapShouldHave3Students() {
        assertEquals(3, this.theMap.size());
    }
    
    
    /**
     * Test case for loading student data.
     */
    @Test
    public void mapShouldContainBarneyRubble() {
        assertTrue(this.theMap.contains("Rubble, Barney"));
    }
    
    /**
     * Test case for loading student data.
     */
    @Test
    public void mapShouldContainFredFlintstone() {
        assertTrue(this.theMap.contains("Flintstone, Fred"));
    }
    
    /**
     * Test case for loading student data.
     */
    @Test
    public void mapShouldContainBruceWayne() {
        assertTrue(this.theMap.contains("Wayne, Bruce"));
    }

}
