/**
 * 
 */
package edu.westga.cs1302.facetutor.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.facetutor.controller.LoadStudentDataController;
import edu.westga.cs1302.facetutor.controller.MatchPictureAndNameController;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;
import edu.westga.cs1302.facetutor.model.StudentDataMap;

/**
 * WhenMatchPictureAndName defines test cases for the "match picture and name"
 * use case.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 *
 */
public class WhenMatchPictureAndName {
    private DataMap<String, StudentData> theMap;
    // private List<String> studentList;
    private MatchPictureAndNameController theController;
    private File testFile;
    private LoadStudentDataController testLoadController;

    /**
     * Creates a new WhenAddAMovie object.
     */
    public WhenMatchPictureAndName() {
        // no-op
    }

    /**
     * Set up method for the test cases.
     * 
     * @throws IOException
     *             if test file can't be read
     */
    @Before
    public void setUp() throws IOException {
        this.theMap = new StudentDataMap();
        this.testFile = new File(getClass().getResource("resources/movies.txt")
                .getFile());
        this.testLoadController = new LoadStudentDataController(this.testFile,
                this.theMap);
        this.testLoadController.loadStudentDataToMap();
        this.theController = new MatchPictureAndNameController(this.theMap);

    }

    /**
     * Test case for matching picture and name.
     */
    @Test
    public void shouldMatchIfNamesTheSame() {
        this.theController.doTheyMatch(
                "http://www.jewornotjew.com/img/people/f/fred_flintstone.jpg",
                "Flintstone, Fred");

        assertTrue(true);
    }

    /**
     * Test case for matching picture and name.
     */
    @Test
    public void shouldNotMatchIfNamesAreDifferent() {
        this.theController.doTheyMatch(
                "http://www.jewornotjew.com/img/people/f/fred_flintstone.jpg",
                "Rubble, Barney");

        assertFalse(false);
    }

}
