/**
 * 
 */
package edu.westga.cs1302.facetutor.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.cs1302.facetutor.controller.GetStudentInLearningModeController;
import edu.westga.cs1302.facetutor.controller.LoadStudentDataController;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;
import edu.westga.cs1302.facetutor.model.StudentDataMap;

/**
 * WhenGetNextStudentInLearningMode defines test cases for the
 * "get next student in learning mode" use case.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 *
 */
public class WhenGetNextStudentInLearningMode {

    private DataMap<String, StudentData> theMap;
    private GetStudentInLearningModeController theController;
    private File testFile;
    private LoadStudentDataController testLoadController;

    /**
     * Creates a new WhenAddAMovie object.
     */
    public WhenGetNextStudentInLearningMode() {
        // no-op
    }

    /**
     * Set up method for the test cases.
     * 
     * @throws IOException
     *             if test file can't be read
     */
    @Before
    public void setUp() throws IOException {
        this.theMap = new StudentDataMap();
        this.testFile = new File(getClass().getResource("resources/movies.txt")
                .getFile());
        this.testLoadController = new LoadStudentDataController(this.testFile,
                this.theMap);
        this.testLoadController.loadStudentDataToMap();
        this.theController = new GetStudentInLearningModeController(this.theMap);

    }

    /**
     * Test case for getting next student.
     */
    @Test
    public void firstStudentAccessedShouldBeFirstStudentInTheList() {
        this.theController.nextStudent();
        assertEquals(
                "https://yt3.ggpht.com/-ObG-8habF2Y/AAAAAAAAAAI/AAAAAAAAAAA/2NB20J2q3w8/s900-c-k-no/photo.jpg",
                this.theMap.get("Rubble, Barney").getPictureUrl());
    }

    /**
     * Test case for getting next student.
     */
    @Test
    public void secondStudentAccessedShouldBeSecondStudentInTheList() {
        this.theController.nextStudent();
        this.theController.nextStudent();
        assertEquals(
                "http://www.jewornotjew.com/img/people/f/fred_flintstone.jpg",
                this.theMap.get("Flintstone, Fred").getPictureUrl());
    }

    /**
     * Test case for getting next student.
     */
    @Test
    public void nplus1StudentAccessedShouldBeFirstStudentInTheList() {
        this.theController.nextStudent();
        this.theController.nextStudent();
        this.theController.nextStudent();
        assertEquals(
                "https://yt3.ggpht.com/-ObG-8habF2Y/AAAAAAAAAAI/AAAAAAAAAAA/2NB20J2q3w8/s900-c-k-no/photo.jpg",
                this.theMap.get("Rubble, Barney").getPictureUrl());
    }
}
