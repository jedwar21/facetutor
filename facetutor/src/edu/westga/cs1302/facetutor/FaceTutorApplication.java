/**
 * 
 */
package edu.westga.cs1302.facetutor;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FaceTutorApplication extends Application to create the GUI and run the program.
 * 
 * @author CS 1302
 * @version 0.5
 */
public class FaceTutorApplication extends Application {
	
	private static final String WINDOW_TITLE = "Face Tutor";
	private static final String GUI_FXML = "view/FaceTutorGui.fxml";

	/**
	 * Constructs a new FaceTutorApplication object.
	 * 
	 * @precondition	none
	 * @postcondition	the object is ready to execute
	 */
	public FaceTutorApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}
	
	
	/**
	 * Launches the application.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
