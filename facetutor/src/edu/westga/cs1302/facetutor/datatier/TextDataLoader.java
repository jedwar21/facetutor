/**
 * 
 */
package edu.westga.cs1302.facetutor.datatier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * TextDataLoader loads lines of text from a text file.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class TextDataLoader {

	private final File aTextFile;

	/**
	 * Creates a new TextDataLoader to input the specified text file.
	 * 
	 * @precondition aTextFile != null
	 * @postcondition the object is ready to read the file
	 * 
	 * @param aTextFile
	 *            the file to read
	 */
	public TextDataLoader(File aTextFile) {
		if (aTextFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.aTextFile = aTextFile;
	}

	/**
	 * Reads all lines from the text file.
	 * 
	 * @precondition none
	 * @return a list of Strings containing the contents of the file
	 * 
	 * @throws IOException
	 *             if the file can't be read
	 */
	public List<String> loadText() throws IOException {
		return Files.readAllLines(this.aTextFile.toPath());
	}

}
