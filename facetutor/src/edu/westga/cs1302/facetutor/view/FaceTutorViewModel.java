/**
 * 
 */
package edu.westga.cs1302.facetutor.view;

import java.io.File;
import java.io.IOException;
//import java.util.List;


import edu.westga.cs1302.facetutor.controller.GetStudentInLearningModeController;
import edu.westga.cs1302.facetutor.controller.LoadStudentDataController;
import edu.westga.cs1302.facetutor.controller.MatchPictureAndNameController;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

/**
 * FaceTutorViewModel mediates between the GUI and the rest of the program.
 * 
 * @author CS 1302
 * @version 0.5
 */
public class FaceTutorViewModel {

    private ObservableList<String> studentList;
    private ObjectProperty<Image> studentFaceImage;
    private DataMap<String, StudentData> theMap;
    private LoadStudentDataController loadStudentDataController;
    private GetStudentInLearningModeController nextStudentInLearningModeController;
    private MatchPictureAndNameController matchPictureAndNameController;
    private StringProperty studentName;
    private StringProperty pictureUrl;

    /**
     * Creates a new FaceTutorViewModel object with its properties initialized.
     * 
     * @precondition none
     * @postcondition the object exists
     */
    public FaceTutorViewModel() {
        Image initialImage = new Image(getClass().getResourceAsStream(
                "blankface.png"));
        this.studentFaceImage = new SimpleObjectProperty<Image>(initialImage);

        this.studentList = FXCollections.observableArrayList();
    }

    /**
     * Returns the property that represents the student image.
     * 
     * @precondition none
     * @return the student image
     */
    public Property<Image> studentImageProperty() {
        return this.studentFaceImage;
    }

    /**
     * Returns the list of names.
     * 
     * @precondition none
     * @return the lines of text
     */
    public ObservableList<String> getStudents() {
        return this.studentList;
    }

    /**
     * Adds the contents of the specified text file to the lines of text.
     * 
     * @precondition inputFile != null
     * @postcondition getStudents() contains all the text read from the file
     * 
     * @param inputFile
     *            a text file
     * @throws IOException
     *             if the file cannot be read
     */
    public void addTextFrom(File inputFile) throws IOException {
        if (inputFile == null) {
            throw new IllegalArgumentException("File to load is null");
        }

        this.loadStudentDataController = new LoadStudentDataController(
                inputFile, this.theMap);
        this.loadStudentDataController.loadStudentDataToMap();

//        this.updateNameList();
    }

//    private void updateNameList() {
//        this.studentList.clear();
//        this.studentList.addAll(this.theMap.keys());
//        this.studentList.sort(null);
//    }

    /**
     * Tells the GetStudentInLearningModeController to go to the next student in the list.
     * 
     * @precondition none
     * @postcondition the next student's picture is displayed.
     */
    public void nextStudent() {
        // TODO Auto-generated method stub
        this.nextStudentInLearningModeController.nextStudent();
    }

    /**
     * Tells the MatchPictureAndNameController to compare the name and picture.
     * 
     * @precondition none
     * @postcondition the name and picture have been compared
     */
    public void selectStudent() {
        // TODO Auto-generated method stub
        this.matchPictureAndNameController.doTheyMatch(this.pictureUrl.toString(), this.studentName.toString());
    }

}
