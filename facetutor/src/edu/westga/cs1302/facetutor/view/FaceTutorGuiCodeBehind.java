/**
 * 
 */
package edu.westga.cs1302.facetutor.view;

/**
 * Defines the code-behind controller for the GUI.
 * 
 * @author CS 1302
 * @version 0.5
 */
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FaceTutorGuiCodeBehind defines the code-behind "controller" for the GUI.
 * 
 * @author CS 1302
 * @version 0.5
 */
public class FaceTutorGuiCodeBehind {
	
	private static final String ERROR_DIALOG_TITLE = "Import error";
    private static final String IO_ERROR_MESSAGE = "ERROR: couldn't import  file";

    private FaceTutorViewModel theViewModel;

    @FXML
    private ListView<String> studentNameListView;

    @FXML
    private Text userInstructions;
    
    @FXML
    private MenuItem openMenuItem;

    @FXML
    private ImageView studentPictureImageView;

    @FXML
    private Button nextStudentButton;
    
    @FXML
    private Button selectButton;
    
    /**
     * FaceTutorGuiCodeBehind creates a new code-behind object and its view model. 
     * 
     * @param   none
     * 
     * @precondition    none
     * 
     * @postcondition   The fields will b e created
     */
    public FaceTutorGuiCodeBehind() {
    	this.theViewModel = new FaceTutorViewModel();
    }
    
    /**
	 * Initializes the GUI components, binding them to the view model properties
	 * and setting their event handlers.
	 * 
	 * @precondition   none
	 * 
	 * @postcondition  The GUI will be initialized
	 */
	@FXML
	public void initialize() {
		this.openMenuItem.setOnAction(event -> this.handleImportAction());
		this.studentPictureImageView.imageProperty().
		            bindBidirectional(this.theViewModel.studentImageProperty());
		this.studentNameListView.setItems(this.theViewModel.getStudents());
        this.nextStudentButton.setOnAction(event -> this.theViewModel.nextStudent());
        this.selectButton.setOnAction(event -> this.theViewModel.selectStudent());
	}
	

    private void handleImportAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}
		
		try {
            this.theViewModel.addTextFrom(inputFile);
            this.studentNameListView.setItems(this.theViewModel.getStudents());
        } catch (IOException theException) {
            JOptionPane.showMessageDialog(null,
                    IO_ERROR_MESSAGE,
                    ERROR_DIALOG_TITLE,
                    JOptionPane.ERROR_MESSAGE);
        } 			
	}
	
	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("Text Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}

}