package edu.westga.cs1302.facetutor.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * StudentDataMap implements the DataMap interface to 
 * store String/StudentData pairs in memory.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class StudentDataMap implements DataMap<String, StudentData> {
	
	private Map<String, StudentData> theMap;
	
	/**
	 * Creates a new MovieDataMap with an empty map.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public StudentDataMap() {
		this.theMap = new HashMap<>();
	}

	@Override
	public void add(String aKey, StudentData aValue) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (aValue == null) {
			throw new IllegalArgumentException("The value was null");
		}
		if (this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is already in the map");
		}
		
		this.theMap.put(aKey, aValue);		
	}
		

	@Override
	public StudentData get(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		if (!this.contains(aKey)) {
			throw new IllegalArgumentException("Key value is not in the map");
		}
		
		return this.theMap.get(aKey);
	}

	@Override
	public boolean contains(String aKey) {
		if (aKey == null) {
			throw new IllegalArgumentException("The key was null");
		}
		
		return this.theMap.containsKey(aKey);
	}

	@Override
	public int size() {
		return this.theMap.size();
	}

	@Override
	public List<StudentData> values() {
		return new ArrayList<StudentData>(this.theMap.values());
	}

	@Override
	public List<String> keys() {
		return new ArrayList<String>(this.theMap.keySet());
	}

}
