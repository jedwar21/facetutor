package edu.westga.cs1302.facetutor.model;

/**
 * StudentData objects represent the name and pictureUrl.
 * 
 * @author CS 1302
 * @version	Spring, 2015
 */
public class StudentData {

	private final String name;
	private final String pictureUrl;
	

	/**
	 * Creates a new StudentData object with the specified
	 * name and pictureUrl.
	 * 
	 * @precondition 	name != null && name.length() > 0 &&
	 * 				    pictureUrl != null && pictureUrl.length() > 0 
	 * 					
	 * @postcondition
	 * 
	 * @param name			      the name of the student
	 * @param pictureUrl		  the URL of the picture
	  */
	public StudentData(String name, String pictureUrl) {
		if (name == null) {
			throw new IllegalArgumentException("Name is null");
		}
		if (name.length() == 0) {
			throw new IllegalArgumentException("Name is empty");
		}
		if (pictureUrl == null) {
			throw new IllegalArgumentException("posterUrl is null");
		}
		if (pictureUrl.length() == 0) {
			throw new IllegalArgumentException("posterUrl is empty");
		}
		
		
		this.name = name;
		this.pictureUrl = pictureUrl;
		
	}

	/**
	 * Returns the name of the movie.
	 * 
	 * @precondition	none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the URL of the poster for the movie.
	 * 
	 * @precondition	none
	 * @return the poster URL
	 */
	public String getPictureUrl() {
		return this.pictureUrl;
	}

	
	
}