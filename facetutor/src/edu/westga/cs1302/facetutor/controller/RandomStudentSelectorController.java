package edu.westga.cs1302.facetutor.controller;

import java.util.List;
import java.util.Random;

import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;

/**
 * CharacterSelector selects pseudorandom students from a list.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 */
public class RandomStudentSelectorController {
	
	private List<String> studentList;
	private DataMap<String, StudentData> theMap;
	private List<String> randomStudentList;
	
	private Random randomizer;
    private List<String> randomSmallStudentList;
	
	/**
	 * Creates a new RandomStudentSelectorController instance whose
	 * random number generator is unseeded.
	 */
	public RandomStudentSelectorController() {
		this.randomizer = new Random();
		this.studentList = this.theMap.keys();
	}

	/**
	 * Creates a new RandomStudentSelectorController instance whose
	 * random number generator is seeded with the
	 * specified value.
	 * 
	 * @param seed	the initial value of the internal state
	 * 				of the pseudorandom number generator
	 */
	public RandomStudentSelectorController(long seed) {
		this.randomizer = new Random(seed);
	}
	
	/**
	 * Displays 10 "random" characters to the console.
	 */
	public void displayTheStudents() {
		for (int i = 0; i < 10; i++) {
			int index = this.randomizer.nextInt(this.studentList.size());
			String randomStudent = (this.studentList.get(index));
			
			this.randomSmallStudentList.add(randomStudent);
			
		}
		this.randomStudentList = this.randomSmallStudentList;
	}
	
	/**
	 * Returns the random list of students
	 * 
	 * @precondition none
	 * @postcondition the random list is accessible
	 * 
	 * @return the list of students that has been randomized
	 */
	public List<String> getRandomStudentList() {
	    return this.randomStudentList;
	}

}
