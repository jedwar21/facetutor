/**
 * 
 */
package edu.westga.cs1302.facetutor.controller;

import java.util.List;

import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;

/**
 * GetStudentInLearningModeController manages the use-case of displaying the
 * next student in the list.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 *
 */
public class GetStudentInLearningModeController {

    private List<String> studentList;
    private DataMap<String, StudentData> theMap;

    /**
     * Creates a new GetStudentInLearningModeController to manage displaying the
     * next student.
     * 
     * @precondition theMap != null
     * @postcondition the controller is ready to display the next student
     * 
     * @param theMap
     *            the data map
     */
    public GetStudentInLearningModeController(
            DataMap<String, StudentData> theMap) {
        if (theMap == null) {
            throw new IllegalArgumentException("Map is null");
        }

        this.theMap = theMap;
        this.studentList = this.theMap.keys();
    }

    /**
     * Goes to the next student in the list and displaying their picture.
     * 
     * @precondition none
     * @postcondition the next student is displayed
     * 
     * @return the picture of the next student
     */
    public String nextStudent() {
        String aStudent = this.moveDownTheList();

        return this.theMap.get(aStudent).getPictureUrl();
    }

    private String moveDownTheList() {
        int index = +1;
        String aStudent = this.studentList.get(index);

        return aStudent;
    }

}
