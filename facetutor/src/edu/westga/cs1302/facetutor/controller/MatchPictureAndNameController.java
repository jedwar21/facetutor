/**
 * 
 */
package edu.westga.cs1302.facetutor.controller;


import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;

/**
 * MatchPictureAndNameController manages the use-case of matching a student
 * shown to the picture shown.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 *
 */
public class MatchPictureAndNameController {
    private DataMap<String, StudentData> theMap;

    /**
     * Creates a new MatchPictureAndNameController to manage matching the
     * student selected to the picture shown.
     * 
     * @precondition map != null
     * @postcondition the controller is ready to add students
     * 
     * @param map
     *            the data map
     */
    public MatchPictureAndNameController(DataMap<String, StudentData> map) {
        if (map == null) {
            throw new IllegalArgumentException("Map is null");
        }

        this.theMap = map;
    }

    /**
     * Checks to see if the student and picture match, returns true if they do
     * or false if they do not.
     * 
     * @precondition pictureUrl.length() > 0 &&
     *                  clickedName.length() > 0
     * @postcondition the picture and name have been compared
     * 
     * @param pictureUrl
     *            the URL of the picture shown
     * @param clickedName
     *            the student clicked from the list
     * @return true if the student and picture match
     */
    public boolean doTheyMatch(String pictureUrl, String clickedName) {

        if (pictureUrl != this.theMap.get(clickedName).getPictureUrl()) {
            return false;
        }
        return true;
        //
    }
}
