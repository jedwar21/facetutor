package edu.westga.cs1302.facetutor.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.westga.cs1302.facetutor.datatier.TextDataLoader;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;


/**
 * AddStudentsFromFileController manages adding student
 * names from a text file.
 * 
 * @author CS 1302
 * @version 0.5
 */
public class AddStudentsFromFileController {

	private TextDataLoader dataLoader;
	private List<String> textFromFile;
	private DataMap<String, StudentData> theMap;

	/**
	 * Creates a new AddTextFromFileController instance.
	 * 
	 * @precondition 	inputFile != null
	 * @postcondition	the object is ready to get the data from the file
	 *   
	 * @param inputFile		the text file to read
	 * @throws IOException  if the file can't be read
	 */
	public AddStudentsFromFileController(File inputFile) throws IOException {
		if (inputFile == null) {
			throw new IllegalArgumentException("File to load is null");
		}
		
		this.dataLoader = new TextDataLoader(inputFile);
		this.textFromFile = this.readTextFromFile();
	}

	/**
	 * Returns the contents of the text file.
	 * 
	 * @precondition 	none
	 * @return			the lines from the text file
	 * 
	 */
	public List<String> getTextLines()  {
		return this.textFromFile;
	}

	private List<String> readTextFromFile() throws IOException {
		return this.dataLoader.loadText();
	} 
	
	/**
     * Adds the specified movie to the map.
     * 
     * @precondition    aMovie != null && !getMap().contains(title)
     * 
     * @postcondition   getMap().contains(title) && 
     *                  getMap.size() == getMap.size()@prev + 1 
     * 
     * @param aStudent    the student to add
     */
    public void addStudent(StudentData aStudent) {
        if (this.theMap.contains(aStudent.getName())) {
            throw new IllegalArgumentException("The student is alreay in the map");
        }
        
        this.theMap.add(aStudent.getName(), aStudent);
    }
}
