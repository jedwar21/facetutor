/**
 * 
 */
package edu.westga.cs1302.facetutor.controller;

import java.io.File;
import java.io.IOException;
//import java.util.ArrayList;
import java.util.List;

import edu.westga.cs1302.facetutor.datatier.TextDataLoader;
import edu.westga.cs1302.facetutor.model.DataMap;
import edu.westga.cs1302.facetutor.model.StudentData;

/**
 * AddstudentController manages the use-case of adding a student to the data
 * map.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class LoadStudentDataController {

    private DataMap<String, StudentData> theMap;
    private File inputFile;
    
    /**
     * Creates a new AddstudentController to manage adding students to the
     * specified map.
     * 
     * @precondition aMap != null
     * @postcondition the controller is ready to add students
     * 
     * @param inputFile
     *            the file to read
     * @param map
     *            the data map
     * @throws IOException
     *             here it is throwing it
     */
    public LoadStudentDataController(File inputFile, DataMap<String, StudentData> map) throws IOException {

        if (inputFile == null) {
            throw new IllegalArgumentException("File to load is null");
        }
        if (map == null) {
            throw new IllegalArgumentException("Map is null");
        }

        this.inputFile = inputFile;
        this.theMap = map;
        
    }

    /**
     * Loads student data from the input file into the map.
     * 
     * @precondition none
     * @postcondition the map contains the student data
     * 
     * @throws IOException
     *             if the file cannot be read
     */
    public void loadStudentDataToMap() throws IOException {
        TextDataLoader loader = new TextDataLoader(this.inputFile);
        List<String> linesFromFile = loader.loadText();

        for (String oneLineFromFile : linesFromFile) {
            this.addStudentToMap(oneLineFromFile);
        }
    }

    private void addStudentToMap(String oneLineFromFile) {
        String[] studentDataArray = oneLineFromFile.split("\\|");
        StudentData data = new StudentData(studentDataArray[0],
                studentDataArray[1]);

        this.theMap.add(data.getName(), data);

    }

   

}
